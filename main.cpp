//
//  main.cpp
//  template_metaprogramming
//
//  Created by Aakrit Prasad on 4/26/13.
//  Copyright (c) 2013 Aakrit Prasad. All rights reserved.
//

#include <iostream>
#include <vector>
using namespace std;

template <int length>
vector<length&> vector<length>::operator+=(const vector<length>& rhs)
{
    for (int i = 0; i < length; ++i)
        value[i] += rhs.valuep[i];
    return *this;
}

template <int N>
struct Factorial
{
    enum { value = N * Factorial<N - 1>::value };
};

template <>
struct Factorial<0> {
    enum { value = 1 };
};

// Factorial<4>::value == 24
// Factorial<0>::value == 1
const int x = Factorial<4>::value; // == 24
const int y = Factorial<0>::value; // == 1

template <class Derived>
struct base
{
    void interface()
    {
        static_cast<Derived*>(this)->implementation();
    }
};



struct derived: base <derived>
{
    void implementation();
};


int main(int argc, const char * argv[])
{

    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}

